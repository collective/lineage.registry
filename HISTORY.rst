
History
=======

1.3 (unreleased)
----------------

- Nothing changed yet.


1.2 (2014-06-06)
----------------

- Fix lineage.registry for sub-subsites and other arbitrary nested sites.
  [thet]

- Added enableRegistry and disableRegistry in order to make event subscribers
  simpler. Now you can enable a local Registry in a simple folder and not only
  on collective.lineage.content.ChildFolder. See: tests.TestLineageRegistry
  [gborelli]


1.1 (2014-01-30)
----------------

- Fix bug when disabling a lineage site.
  [thet]


1.0.1
-----

- Wrong information in README.rst corrected.
  [jensens]


1.0
---

- Make it work [jensens, 2012-01-25]
